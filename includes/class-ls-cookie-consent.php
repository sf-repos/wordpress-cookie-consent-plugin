<?php
/**
* Class to handle the admin settings page
*/
class Ls_Cookie_Consent {

    function __construct() {
        add_action( 'admin_menu', array( &$this , 'init_admin_submenu' ) ); //Adds submenu page
    }

    /**
    * Submenu page init settings
    */
    function init_admin_submenu() {
        add_submenu_page( 'options-general.php', 'Cookie Notice', 'Cookie Notice', 'manage_options', 'cookie_consent_notice', array( &$this , 'cookie_consent_notice_admin_page' ) );
    }

    /**
    * Options updated message
    */
    function ls_cookie_consent_options_saved_notice() {
        ?>
        <div class="notice notice-success is-dismissible">
            <p>Your settings have been successfully saved.</p>
        </div>
        <?php
    }

    /**
    * Options remain the same but they clicked save anyway message
    */
    function ls_cookie_consent_options_not_updated_notice() {
        ?>
        <div class="notice notice-success is-dismissible">
            <p>No settings needed to be changed.</p>
        </div>
        <?php
    }

    /**
    * Form Page display and update function
    */
    function cookie_consent_notice_admin_page() {
        //array containing variables we are using
        $option_names = array(
            'message',
            'button_message',
            'learn_more_link',
            'learn_more_message',
            'position',
            'banner_colour',
            'button_colour',
            'banner_text',
            'button_text',
        );
        $options = array();
        $success = '';

        //Update the DB if $_POST is set
        if ( !empty( $_POST ) ){

            foreach ($option_names as $option_name) {
            
                $options[$option_name] = $_POST[$option_name]; //check we are saving only the allowed variables, not any extra dodgy ones...
            }

            $success = update_option( 'ls_cookie_consent_options', $options ); //save the data using the wordpress function which santitises for us too

            if($success == TRUE){
                $this->ls_cookie_consent_options_saved_notice();
            } else {
                $this->ls_cookie_consent_options_not_updated_notice();
            }
        }

        $db_options = get_option( 'ls_cookie_consent_options' ); //Get the DB options

        foreach ($option_names as $option_name) {
            if (!isset($db_options[$option_name]) ) {
                $options[$option_name] = ''; //if any variables aren't set, declare them blank
            } else {
                $options[$option_name] = $db_options[$option_name];
            }
        }
        ?>
        <div class="wrap">
        <h1>Cookie Consent Notice</h1>

        <form method="post" action="options-general.php?page=cookie_consent_notice">
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row">
                            <label for="message">Message</label>
                        </th>
                        <td>
                            <textarea rows="3" cols="18" cols="50" name="message" id="message" class="postform" placeholder="This website uses cookies to ensure you get the best experience on our website."><?php echo stripslashes($options['message']); ?></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="button_message">Button Text</label>
                        </th>
                        <td>
                            <input type="text" name="button_message" id="button_message" class="postform" placeholder="Got it!" value="<?php echo stripslashes($options['button_message']); ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="learn_more_link">Learn More Link</label>
                        </th>
                        <td>
                            <input type="text" name="learn_more_link" id="learn_more_link" class="postform" placeholder="https://cookiesandyou.com/" value="<?php echo stripslashes($options['learn_more_link']); ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="learn_more_message">Learn More Message</label>
                        </th>
                        <td>
                            <input type="text" name="learn_more_message" id="learn_more_message" class="postform" placeholder="Learn more" value="<?php echo stripslashes($options['learn_more_message']); ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="position">Position</label>
                        </th>
                        <td>
                            <select name="position" id="position" class="postform">
                                <option class="level-0" value="" <?php if( empty($options['position']) ){ echo 'selected'; }?> >Banner bottom</option>
                                <option class="level-0" value="top" <?php if( "top" == $options['position'] ){ echo 'selected'; }?> >Banner top</option>
                                <option class="level-0" value="top-static" <?php if( "top-static" == $options['position']){ echo 'selected'; }?> >Banner top (pushdown)</option>
                                <option class="level-0" value="bottom-left" <?php if( "bottom-left" == $options['position']){ echo 'selected'; }?> >Floating left</option>
                                <option class="level-0" value="bottom-right" <?php if( "bottom-right" == $options['position']){ echo 'selected'; }?> >Floating right</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="banner_colour">Banner Colour</label>
                        </th>
                        <td>
                            <input type="text" name="banner_colour" id="banner_colour" class="color-picker" data-alpha="true" data-default-color="#edeff5" value="<?php echo $options['banner_colour']; ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="banner_text">Banner Text</label>
                        </th>
                        <td>
                            <input type="text" name="banner_text" id="banner_text" class="color-picker" data-alpha="false" data-default-color="#838391" value="<?php echo $options['banner_text']; ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="button_colour">Button Colour</label>
                        </th>
                        <td>
                            <input type="text" name="button_colour" id="button_colour" class="color-picker" data-alpha="false" data-default-color="#4b81e8" value="<?php echo $options['button_colour']; ?>"></input>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">
                            <label for="button_text">Button Text</label>
                        </th>
                        <td>
                            <input type="text" name="button_text" id="button_text" class="color-picker" data-alpha="false" data-default-color="#ffffff" value="<?php echo $options['button_text']; ?>"></input>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p class="submit"><input name="submit" id="submit" class="button button-primary" value="Save Changes" type="submit"></p>
        </form>
        <?php
    }
}
