<?php
/*
Plugin Name: LS Cookie Consent
Plugin URI: https://bitbucket.org/sf-repos/wordpress-cookie-consent-plugin/
Description: Display a cookie policy notice on your website.
Version: 1.0.3
Author: Sam Fullalove
Author URI: http://sam.fullalove.co/
Text Domain: ls-cookie-consent
License: GPLv2
*/

/*
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301
USA
*/

/**
* Run this code when plugin is activated
*/
function ls_cookie_consent_activation() {
    global $wp_version;

    //make sure running minimum wordpress version required to use wp functions
    if ( version_compare( $wp_version, '4.1', '<' ) ) {
        wp_die( 'This plugin requires WordPress version 4.1 or higher.' );
    }
}
register_activation_hook( __FILE__, 'ls_cookie_consent_activation' );

/**
* Run this code when plugin is deactivated
*/
function ls_cookie_consent_deactivate() {
    //do something
}
register_deactivation_hook( __FILE__, 'ls_cookie_consent_deactivate()' );

/**
* Enqueue the js files needed for WIDGET ADMIN pages (customiser and widget page)
*/
function ls_cookie_consent_enqueue_js( $hook ) {
    //if not editing the cookie_consent settings, end the function
    if ( $hook !== 'settings_page_cookie_consent_notice' ) {
        return;
    }

    wp_enqueue_style( 'wp-color-picker' ); //without these color picker items the color picker won't work...
    wp_enqueue_script( 'alpha-color-picker', plugin_dir_url( __FILE__ ) . 'js/alpha-color-picker.js', array('jquery', 'wp-color-picker'), FALSE, TRUE); //include the transparent color picker.
}
add_action('admin_enqueue_scripts', 'ls_cookie_consent_enqueue_js');

/**
* Enqueue cookie_consent JS code
*/
function ls_cookie_consent_scripts() {
    wp_enqueue_style( 'ls-cookie-consent-css', plugin_dir_url( __FILE__ ) . 'css/cookie-consent.css' ); //include cookie consent css
    wp_enqueue_script( 'ls-cookie-consent', plugin_dir_url( __FILE__ ) . 'js/cookie-consent.js', FALSE, FALSE, TRUE ); //include cookie consent js

    $options = get_option( 'ls_cookie_consent_options' );

    if( ! empty( $options['position'] ) ) {
       $position = ',"position": "' . $options['position'] . '"';
           if( 'top-static' == $options['position'] ) {
              $position = ',"position": "top","static": true';
           }
    } else {
       $position = '';
    }

    if( ! empty( $options['banner_colour'] ) ) {
        $banner_colour = '"background":"' . $options['banner_colour'] . '"';
    } else {
        $banner_colour = '"background": "#edeff5"';
    }

    if( ! empty( $options['banner_text'] ) ) {
        $banner_text = ',"text":"' . $options['banner_text'] . '"';
    } else {
        $banner_text = ',"text": "#838391"';
    }

    if( ! empty( $options['button_colour'] ) ) {
        $button_colour = '"background":"' . $options['button_colour'] . '"';
    } else {
        $button_colour = '"background": "#4b81e8"';
    }

    if( ! empty( $options['button_text'] ) ) {
        $button_text = ',"text":"' . $options['button_text'] . '"';
    } else {
        $button_text = '';
    }

    if( ! empty( $options['learn_more_link'] ) ) {
        $learn_more_link = '"href": "' . $options['learn_more_link'] . '",';
    } else {
        $learn_more_link = '';
    }

    if( ! empty( $options['message'] ) ) {
        $message = '"message": "' . $options['message'] . '",';
    } else {
        $message = '';
    }

    if( ! empty( $options['button_message'] ) ) {
        $button_message = '"dismiss": "' . $options['button_message'] . '",';
    } else {
        $button_message = '';
    }

    if( ! empty( $options['learn_more_message'] ) ) {
        $learn_more_message = '"link": "' . $options['learn_more_message'] . '",';
    } else {
        $learn_more_message = '';
    }

    if( !empty( $learn_more_link ) || ! empty( $message ) || ! empty( $button_message ) || ! empty( $learn_more_message ) ) {
        $content = ',"content": {' . $learn_more_link . $message . $button_message . $learn_more_message . '}';
    } else {
        $content = '';
    }

    $custom_js =
        'window.addEventListener("load", function(){
            window.cookieconsent.initialise({
                "palette": {
                  "popup": {
                    '.$banner_colour.'
                    '.$banner_text.'
                  },
                  "button": {
                    '.$button_colour.'
                    '.$button_text.'
                  }
                }
              ,"theme": "classic"
              '. $position . $content .'
            })
        });
    ';
    wp_add_inline_script( 'ls-cookie-consent', $custom_js );
}
add_action( 'wp_enqueue_scripts', 'ls_cookie_consent_scripts', 11 ); //needs to have higher priority so loads after GP default inline styles

require_once('includes/class-ls-cookie-consent.php');
$ls_cookie_consent = new Ls_Cookie_Consent(); //triggers the admin end code
