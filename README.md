# Cookie Consent

I use cookieconsent by osano to create the cookie box on the front end using javascript:
https://github.com/osano/cookieconsent
Also, I'm using wp-color-picker-alpha by kallookoo to allow transparency of the cookie box.
https://github.com/kallookoo/wp-color-picker-alpha

This Wordpress plugin is designed to display a simple message on your website telling your users that you use cookies. 
You can easily customise the messages and buttons using the admin area of wordpress under Settings->Cookie Notice.

To use, download as a zip and upload and activate it directly from the wordpress admina area.

DISCLAIMER: Please note that this plugin by itself will not control cookies or make your website compliant with cookie laws or data protection laws, it is only a simple visual warning for users.

Default style:

![picture](https://bitbucket.org/sf-repos/wordpress-cookie-consent-plugin/raw/master/screenshots/default.png)

Customisation Settings:

![picture](https://bitbucket.org/sf-repos/wordpress-cookie-consent-plugin/raw/master/screenshots/settings.png)

Example of custom style:

![picture](https://bitbucket.org/sf-repos/wordpress-cookie-consent-plugin/raw/master/screenshots/custom.png)